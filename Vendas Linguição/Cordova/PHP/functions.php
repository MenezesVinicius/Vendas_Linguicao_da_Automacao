<?php
    require_once('mysqli.php');

    class Functions
    {
        private $MySQLi;

        public function __construct()
        {
            $this->MySQLi = new Connect();
        }

        public function UpdateVendedorLocation($cpf, $latitude, $longitude)
        {
            $sql = "UPDATE DadosVendedor SET latitude = '$latitude', longitude = '$longitude' WHERE cpf_vendedor = $cpf";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);

            if ($result)
            {
                return $result;
            }
            else
            {
                return false;
            }
        }

        public function ChecarExistencia($cpf)
        {
            $sql = "SELECT nome from Vendedores WHERE cpf = $cpf";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);
            $no_of_rows = $result->num_rows;

            if ($no_of_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function ListarVendedores()
        {
            $sql = "SELECT id, latitude, longitude, nome from DadosVendedor ORDER BY nome";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);
            $response["vendedores"] = array();

            while ($row = $result->fetch_assoc())
            {
                $vendedor = array();
                $vendedor["id"] = $row["id"];
                $vendedor["nome"] = $row["nome"];
                $vendedor["latitude"] = $row["latitude"];
                $vendedor["longitude"] = $row["longitude"];
                // push single product into final response array
                array_push($response["vendedores"], $vendedor);
            }
            // success
            $response["success"] = 1;

            return $response;
        }
    }