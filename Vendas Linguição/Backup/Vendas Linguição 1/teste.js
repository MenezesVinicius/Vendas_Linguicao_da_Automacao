$(function () {
    $('#btnVendedores').click(function () {
        if ($('#btnOrganizadores').is(':visible')) {
            $('#org').hide(400);
        }
        $('#login').slideToggle(400);
    });

    $('#btnOrganizadores').click(function () {
        if ($('#btnVendedores').is(':visible')) {
            $('#login').hide(400);
        }
        $('#org').slideToggle(400);
    });

    $(".dropdownbox").click(function () {
        $(".menu").toggleClass("showMenu");
        $(".menu > li").click(function () {
            $(".dropdownbox > p").text($(this).text());
            $(".menu").removeClass("showMenu");
        });
    });
});