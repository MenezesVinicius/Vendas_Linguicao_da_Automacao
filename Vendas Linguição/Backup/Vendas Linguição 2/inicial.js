var btnAddVendedor = '#btnAddVendedor';
var divImagem = '#divImagem';
var imageUpload = '#imageUpload';

$(function () {
    $(btnAddVendedor).click(function () {

    });

    $(divImagem).click(function() {
        $(imageUpload).click();
    });

    //file input preview
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]); // read the local file

            reader.onloadend = function(){ // set image data as background of div
                $("#divImagem").css("background-image", "url("+this.result+")");
                alert($(this).width() + 'x' + $(this).height());
            }
        }
    }
    $("input:file").change(function(){
        readURL(this);
    });
});
