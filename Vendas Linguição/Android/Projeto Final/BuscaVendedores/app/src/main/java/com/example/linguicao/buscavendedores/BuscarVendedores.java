package com.example.linguicao.buscavendedores;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.glomadrian.dashedcircularprogress.DashedCircularProgress;
import com.github.glomadrian.dashedcircularprogress.utils.ViewPagerTransformer;


public class BuscarVendedores extends ActionBarActivity
{
    private DashedCircularProgress dashedCircularProgress;
    private ImageView androidImage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_vendedores);
        dashedCircularProgress = (DashedCircularProgress)findViewById(R.id.size);
        androidImage = (ImageView)findViewById(R.id.android_image);
        dashedCircularProgress.setInterpolator(new AccelerateInterpolator());
        dashedCircularProgress.setValue(999);
        dashedCircularProgress.setOnValueChangeListener(new DashedCircularProgress.OnValueChangeListener() {
            @Override
            public void onValueChange(float value) {
                androidImage.setScaleX(dashedCircularProgress.getScaleX() + value / 900);
                androidImage.setScaleY(dashedCircularProgress.getScaleY() + value / 900);
                if(value == 999.0)
                {
                    Toast.makeText(getApplicationContext(), "" + value, Toast.LENGTH_LONG).show();

                    dashedCircularProgress.reset();
                    dashedCircularProgress.setValue(999);
                }
            }
        });
    }
}
