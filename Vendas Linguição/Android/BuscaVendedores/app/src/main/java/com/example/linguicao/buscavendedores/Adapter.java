package com.example.linguicao.buscavendedores;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.linguicao.buscavendedores.fragment.Size;
import com.example.linguicao.buscavendedores.fragment.DragonBall;
import com.example.linguicao.buscavendedores.fragment.Simple;
import com.example.linguicao.buscavendedores.fragment.Pager;

/**
 * @author Adrián García Lomas
 */
public class Adapter extends FragmentPagerAdapter {
    private static final int COUNT = 4;

    public Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Simple.getInstance();
            case 1:
                return Pager.getInstance();
            case 2:
                return Size.getInstance();
            case 3:
                return DragonBall.getInstance();
        }
        return Simple.getInstance();
    }

    @Override
    public int getCount() {
        return COUNT;
    }
}
