<?php
    require_once('mysqli.php');

    class Functions
    {
        private $MySQLi;

        public function __construct()
        {
            $this->MySQLi = new Connect();
        }

        public function UpdateVendedorLocation($cpf, $latitude, $longitude)
        {
            $sql = "UPDATE DadosVendedor SET latitude = '$latitude', longitude = '$longitude' WHERE cpf_vendedor = $cpf";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);

            if ($result)
            {
                return $result;
            }
            else
            {
                return false;
            }
        }

        public function ChecarExistencia($cpf)
        {
            $sql = "SELECT nome from Vendedores WHERE cpf = $cpf";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);
            $no_of_rows = $result->num_rows;

            if ($no_of_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function ListarVendedores()
        {
            $sql = "SELECT Vendedores.nome, Vendedores.ddd, Vendedores.celular, Vendedores.foto,
                    DadosVendedor.id, DadosVendedor.latitude, DadosVendedor.longitude, DadosVendedor.descricao,
                    DadosVendedor.status
                    FROM Vendedores JOIN DadosVendedor
                    ON Vendedores.cpf = DadosVendedor.cpf_vendedor";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);
            $no_of_rows = $result->num_rows;

            if ($no_of_rows > 0)
            {
                $response["vendedores"] = array();
                while ($row = $result->fetch_assoc())
                {
                    $vendedor = array();
                    $vendedor["id"] = $row["id"];
                    $vendedor["nome"] = $row["nome"];
                    $vendedor["latitude"] = $row["latitude"];
                    $vendedor["longitude"] = $row["longitude"];
                    $vendedor["descricao"] = $row["descricao"];
                    $vendedor["ddd"] = $row["ddd"];
                    $vendedor["celular"] = $row["celular"];
                    $vendedor["foto"] = $row["foto"];
                    $vendedor["status"] = $row["status"];
                    // push single product into final response array
                    array_push($response["vendedores"], $vendedor);
                }
                // success
                $response["success"] = 1;
            }
            else
            {
                $response["erro"] = 2;
            }
            return $response;
        }
    }