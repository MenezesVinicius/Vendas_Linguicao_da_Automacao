var myLat = 0;
var myLng = 0;
var app = angular.module('starter.controllers', ['ionic', 'ngCordova']);

app.controller('appCtrl', function ($scope)
{
});

app.controller('listarVendedoresCtrl', function ($scope, $localStorage, VendedoresService, $rootScope, $ionicPlatform, $cordovaGeolocation)
{
    $scope.GetVendedores = function ()
    {
        $rootScope.show();
        var posOptions = {timeout: 10000, enableHighAccuracy: true};
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position)
        {
            myLat = position.coords.latitude;
            myLng = position.coords.longitude;

            VendedoresService.findAll().then(function (vendedores)
            {
                var vendedores = vendedores.data.vendedores;
                $scope.vendedores = vendedores;
                $scope.$broadcast('scroll.refreshComplete');
                $localStorage.setObject("vendedores", vendedores);
                $rootScope.hide();
            }, function (err)
            {
                console.log(err);
                $scope.vendedores = $localStorage.getObject("vendedores");
                $rootScope.hide();
            })
        }, function (err)
        {
            console.log(err);
            $rootScope.hide();
        });
    };

    $scope.doRefresh = function ()
    {
        $scope.GetVendedores();
        $scope.$broadcast('scroll.refreshComplete');
    };

    $ionicPlatform.ready(function ()
    {
        $scope.GetVendedores();
    });
});

app.controller('vendedorCtrl', function ($scope, $stateParams, VendedoresService, $cordovaSms)
{
    console.log($stateParams.vendedorId);
    $scope.vendedor = VendedoresService.findById($stateParams.vendedorId);

    var tel = $scope.vendedor.ddd.concat($scope.vendedor.celular);

    if ($scope.vendedor.status == 1)
    {
        var zoom = 0;
        if ($scope.vendedor.distancia > 500)
        {
            zoom = 15;
        }
        else if ($scope.vendedor.distancia < 500 && $scope.vendedor.distancia > 100)
        {
            zoom = 16;
        }
        else
        {
            zoom = 17;
        }
        var myLatLng = new google.maps.LatLng(myLat, myLng);
        var vendedorLatLng = new google.maps.LatLng($scope.vendedor.latitude, $scope.vendedor.longitude);
        var mapOptions = {
            center: myLatLng,
            zoom: zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var myLocation = new google.maps.Marker({
            position: myLatLng,
            map: map
        });
        var vendedorLocation = new google.maps.Marker({
            position: vendedorLatLng,
            map: map
        });
        $scope.map = map;
    }
    else
    {
        document.getElementById("map").style.display = 'none';
    }

    $scope.SMS = function ()
    {
        var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
            }
        };
        $cordovaSms.send(tel, 'Oi, ' + $scope.vendedor.nome, options).then(function ()
        {}, function (error)
        {
            console.log(error);
        });
    };

    $scope.OpenWhats = function ()
    {
        cordova.plugins.Whatsapp.send($scope.vendedor.ddd + $scope.vendedor.celular);
    }
});

