// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

    .run(function ($ionicPlatform, $rootScope, $ionicLoading)
    {
        $ionicPlatform.ready(function ()
        {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard)
            {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar)
            {
                StatusBar.styleDefault();
            }

            $rootScope.show = function() {
                $ionicLoading.show({
                    template: '<ion-spinner></ion-spinner>',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 500,
                    showDelay: 0
                });
            };

            $rootScope.hide = function() {
                $ionicLoading.hide();
            };
        });
    })

    .config(function ($stateProvider, $urlRouterProvider)
    {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'appCtrl'
            })

            .state('app.paginaTeste', {
                url: "/paginaTeste",
                views: {
                    'menuContent': {
                        templateUrl: "templates/paginaTeste.html"
                    }
                }
            })

            .state('app.listaVendedores', {
                url: "/listaVendedores",
                views: {
                    'menuContent': {
                        templateUrl: "templates/listaVendedores.html",
                        controller: 'listarVendedoresCtrl'
                    }
                }
            })

            .state('app.vendedor', {
                url: "/listaVendedores/:vendedorId",
                views: {
                    'menuContent': {
                        templateUrl: "templates/vendedor.html",
                        controller: 'vendedorCtrl'
                    }
                }
            });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/paginaTeste');
    });
