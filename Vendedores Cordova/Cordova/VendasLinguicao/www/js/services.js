var myLat = 0;
var myLng = 0;
var app = angular.module('starter.services', ['ionic', 'ngCordova']);

/**
 * @return {number}
 */
function CalcRadiusDistance(lat1, lon1, lat2, lon2)
{
    var RADIUSKILOMETERS = 6373;
    var latR1 = lat1 * Math.PI / 180;
    var lonR1 = lon1 * Math.PI / 180;
    var latR2 = lat2 * Math.PI / 180;
    var lonR2 = lon2 * Math.PI / 180;
    var latDifference = latR2 - latR1;
    var lonDifference = lonR2 - lonR1;
    var a = Math.pow(Math.sin(latDifference / 2), 2) + Math.cos(latR1) * Math.cos(latR2) * Math.pow(Math.sin(lonDifference / 2), 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(c * RADIUSKILOMETERS * 1000);
}

app.factory('$localStorage', ['$window', function($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    }
}]);

app.factory('VendedoresService', function ($rootScope, $http, $ionicLoading, $cordovaToast)
{
    var Vendedores = [];

    return {
        findAll: function ()
        {
            var promise = $http({
                method: 'post',
                url: 'http://cordova.besaba.com/',
                data: {
                    tag: 'listar_vendedores'
                },
                headers: {"Content-Type": 'application/x-www-form-urlencoded'}
            });

            promise.success(function (data)
            {
                if (data.success == 1)
                {
                    for (var i = 0; i < data.vendedores.length; i++)
                    {
                        var vendedor = data.vendedores[i];
                        distancia = CalcRadiusDistance(myLat, myLng, vendedor.latitude, vendedor.longitude);
                        vendedor.distancia = distancia;
                    }
                    Vendedores = data.vendedores;
                }
                else
                {
                    console.log("Vendedores indisponiveis");
                }
            }).error(function (err)
            {
                console.log(err);
                $cordovaToast.show('Sem conexão! Os dados podem estar desatualizados!', 'long', 'bottom');
            });
            return promise;
        },

        findById: function (vendedorId)
        {
            for (var i = 0; i < Vendedores.length; i++)
            {
                if (Vendedores[i].id == vendedorId)
                {
                    var vendedor = Vendedores[i];
                    if (vendedor.ddd == 48)
                    {
                        vendedor.ddd = "";
                    }
                    else if (vendedor.ddd != "" && vendedor.ddd.length == 2)
                    {
                        vendedor.ddd = "041" + vendedor.ddd;
                    }

                    console.log(vendedor.nome);
                    return vendedor;
                }
            }
        }
    }
});