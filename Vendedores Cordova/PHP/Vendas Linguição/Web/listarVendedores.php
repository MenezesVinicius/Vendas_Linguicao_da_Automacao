<?php
    // include db handler
    require_once 'functions.php';
    $function = new Functions();
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="estilos.css">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
    <title></title>
</head>
<body>
<div id="divListarVendedores">
    <?php
        $function->ListarVendedores();
    ?>
</div>
</body>
</html>

