$(function () {
    $("#btnVisualizarVendedores").bind("click", VisualizarVendedores);
    $(".btnDelete").bind("click", Delete);

    function VisualizarVendedores() {
        location.href = "http://vendedoreslinguicao.besaba.com/Web/listarVendedores.php";
    }

    function Delete() {
        var par = $(this).parent().parent();
        par.remove();
        var trid = $(this).closest('tr').attr('id');

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: "deletarVendedor.php", //Where to make Ajax calls
            dataType: "text", // Data type, HTML, json etc.
            data: 'delete_id='+trid, //Form variables
            success: function (response) {
                alert("Vendedor excluido com sucesso!");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });

    }
});