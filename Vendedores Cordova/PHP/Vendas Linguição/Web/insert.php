<meta charset="UTF-8">
<?php
    $nome = $_POST['nome'];
    $cpf = $_POST['cpf'];
    $ddd = $_POST['ddd'];
    $celular = $_POST['cel'];

    // include db handler
    require_once 'functions.php';
    $function = new Functions();

    // response Array
    $response = array("success" => 0, "error" => 0);

    $checaVendedor = $function->ChecarExistencia($cpf);

    if ($checaVendedor == false)
    {
        $vendedor = $function->CadastraVendedor($nome, $cpf, $ddd, $celular);

        if ($vendedor != false)
        {
            $response["success"] = 1;
            echo
            "<script>
                alert('Vendedor cadastrado com sucesso!');
                window.location.href = 'http://vendedoreslinguicao.besaba.com';
            </script>";
        }
        else
        {
            "<script>
                alert('Falha ao cadastrar vendedor! Tente Novamente');
                window.location.href = 'http://vendedoreslinguicao.besaba.com';
            </script>";
            $response["error"] = 1;
        }
    }
    else
    {
        $response["error"] = 2;
        echo
        "<script>
            alert('Vendedor já se encontra cadastrado!');
            window.location.href = 'http://vendedoreslinguicao.besaba.com';
        </script>";
    }
