var salesControllers = angular.module('sales.controllers', ['sales.services', 'helpers.dataloader', 'ionic', 'ngCordova'])
var vendedores_display = [];
var vendedores = [];
/*--------------------------------Lógica para a tela de vendas de ingresso--------------------------------*/
salesControllers.controller('SalesEventosController', function ($scope, DataLoader) {
    DataLoader.load('data/sales/eventos.json').then(function (response) {
            $scope.eventos = response;
        }
    );
});

salesControllers.controller('SalesPontosController', function ($scope, DataLoader) {
    DataLoader.load('data/sales/pontos.json').then(function (response) {
            $scope.pontos = response;
        }
    );
});

salesControllers.controller('VendedoresController', function ($scope, VendedoresService, $rootScope, $ionicPlatform, $ionicScrollDelegate, $cordovaGeolocation) {
    $scope.GetVendedores = function () {
        $rootScope.show();
        var posOptions = {timeout: 5000, enableHighAccuracy: true};
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            myLat = position.coords.latitude;
            myLng = position.coords.longitude;
            console.log(myLat, myLng);

            VendedoresService.findAll().then(function (v) {
                console.log(v);
                vendedores = v;
                $scope.vendedores = vendedores;
                VendedoresService.getUniversidades().then(function (universidades) {
                    $scope.universidades = universidades;
                });
                $rootScope.hide();
            }, function (err) {
                console.log("Erro ao listar vendedores: " + err.message);
                GetVendedoresOffline();
                $rootScope.hide();
            })
        }, function (err) {
            console.log("Erro ao pegar posicao: " + err);
            GetVendedoresOffline();
            $rootScope.hide();
        });
    };

    function GetVendedoresOffline() {
        VendedoresService.getVendedoresOff().then(function (v) {
            vendedores = v;
            $scope.vendedores = vendedores;
            VendedoresService.getUniversidades().then(function (universidades) {
                $scope.universidades = universidades;
            });
        })
    };

    $scope.$watch('search.universidade', function (s) {
        $ionicScrollDelegate.scrollTop(false);
        if (s == "") {
            vendedores_display = vendedores;
        }
        else {
            console.log(vendedores);
            vendedores_display = vendedores.filter(function (v) {
                return v.universidade.search(s) >= 0;
            })
        }
        $scope.vendedores = vendedores_display;
        $ionicScrollDelegate.scrollTop(true);
    });

    $ionicPlatform.ready(function () {
        //if (angular.equals([], vendedores)) {
        //    $scope.GetVendedores();
        //}
        $scope.GetVendedores();
    });
});

salesControllers.controller('VendedorCtrl', function ($scope, $stateParams, VendedoresService, $cordovaSms, $ionicPlatform) {
    var tel;
    $scope.getVendedor = function () {
        $scope.vendedor = VendedoresService.findById($stateParams.vendedorId);
        tel = $scope.vendedor.celular + $scope.vendedor.ddd;

        if ($scope.vendedor.status == 1) {
            //Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', {
            //    callback: function () {
            //        var mapOptions = {
            //            credentials: "AoUa2WjsKc0RGcMPR4nANTELqx3dv5hWic8KsETXkXsDYQyw5TnAbwibSIiOZCV6",
            //            enableClickableLogo: false,
            //            enableSearchLogo: false,
            //            showCopyright: false,
            //            showDashboard: false,
            //            mapTypeId: Microsoft.Maps.MapTypeId.road
            //        };
            //        var map = new Microsoft.Maps.Map(document.getElementById("map"), mapOptions);
            //        // Define the pushpin location
            //        var vendedorLatLng = new Microsoft.Maps.Location($scope.vendedor.latitude, $scope.vendedor.longitude);
            //        var myLatLng = new Microsoft.Maps.Location(myLat, myLng);
            //        // Add a pin to the map
            //        var pin = new Microsoft.Maps.Pushpin(vendedorLatLng);
            //        var pin2 = new Microsoft.Maps.Pushpin(myLatLng);
            //        map.entities.push(pin);
            //        map.entities.push(pin2);
            //
            //        // Center the map on the location
            //        var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(vendedorLatLng, myLatLng);
            //        map.setView({bounds: viewBoundaries});
            //        $scope.map = map;
            //    }
            //})
        }
        else {
            document.getElementById("map").style.display = 'none';
        }
    }

    $scope.SMS = function () {
        var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
            }
        };
        $cordovaSms.send(tel, 'Oi, ' + $scope.vendedor.nome, options).then(function () {
        }, function (error) {
            console.log(error);
        });
    };

    $scope.OpenWhats = function () {
        cordova.plugins.Whatsapp.send($scope.vendedor.ddd + $scope.vendedor.celular);
    }

    $ionicPlatform.ready(function () {
        $scope.getVendedor();
    });
});

