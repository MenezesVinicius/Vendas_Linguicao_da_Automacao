var app = angular.module('starter', ["ionic", "ngCordova", "firebase", 'sales.services',
    'sales.controllers',
    'shows.controllers',
    'shows.services',
    'bar.controllers',
    'bar.services',
    'home.controllers',
    'menu.controller',
    'devs.controllers',
    'helpers.dataloader',
    'social.controllers']);

app.run(function ($ionicPlatform, $rootScope, $ionicLoading) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        $rootScope.show = function () {
            $ionicLoading.show({
                template: "<ion-spinner></ion-spinner>",
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 500,
                showDelay: 0
            });
        };

        $rootScope.hide = function () {
            $ionicLoading.hide();
        };
    });
});

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu/html/menu.html",
        })

        .state('app.home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "templates/home/html/home.html",
                    controller: "HomeController"
                }
            }
        })

        .state('app.map', {
            url: "/map",
            views: {
                'menuContent': {
                    templateUrl: "templates/map/html/map.html",
                }
            }
        })

        .state('app.shows', {
            url: "/shows",
            views: {
                'menuContent': {
                    templateUrl: "templates/shows/html/shows.html",
                    controller: "ShowsController"
                }
            }
        })

        .state('app.bar', {
            url: "/bar",
            views: {
                'menuContent': {
                    templateUrl: "templates/bar/html/bar.html",
                    controller: "BarController"
                }
            }
        })

        .state('app.sales', {
            url: "/sales",
            views: {
                'menuContent': {
                    templateUrl: "templates/sales/html/sales.html",
                    controller: "VendedoresController"
                }
            }
        })

        .state('app.vendedor', {
            url: "/sales/:vendedorId",
            views: {
                'menuContent': {
                    templateUrl: "templates/sales/html/vendedor.html",
                    controller: 'VendedorCtrl'
                }
            }
        })

        .state('app.pontosfixos', {
            url: "/sales/pontosfixos",
            views: {
                'menuContent': {
                    templateUrl: "templates/sales/html/pontos.html",
                    controller: "SalesPontosController"
                }
            }
        })

        .state('app.facebook', {
            url: "/facebook",
            views: {
                'menuContent': {
                    templateUrl: "templates/facebook/html/facebook.html"
                }
            }
        })

        .state('app.eventos', {
            url: "/sales/eventos",
            views: {
                'menuContent': {
                    templateUrl: "templates/sales/html/eventos.html",
                    controller: "SalesEventosController"
                }
            }
        })

        .state('app.devs', {
            url: "/devs",
            views: {
                'menuContent': {
                    templateUrl: "templates/devs/html/devs.html",
                    controller: "DevsController"
                }
            }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});

app.config(['$ionicConfigProvider', function ($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom'); //other values: top
}]);
