angular.module("helpers.dataloader", []).factory('DataLoader',
    function ($q, $http)
    {
        return {
            load: function (file)
            {
                var defer = $q.defer();

                $http.get(file)
                    .success(function (data)
                    {
                        defer.resolve(data);
                    })
                    .error(function ()
                    {
                        defer.reject('could not find ' + file);
                    });
                return defer.promise;
            }
        }
    })