var salesServices = angular.module('sales.services', ['ionic', 'ngCordova', 'firebase']);
var myLat = 0;
var myLng = 0;

/**
 * @return {number}
 */
function CalcRadiusDistance(lat1, lon1, lat2, lon2) {
    var RADIUSKILOMETERS = 6373;
    var latR1 = lat1 * Math.PI / 180;
    var lonR1 = lon1 * Math.PI / 180;
    var latR2 = lat2 * Math.PI / 180;
    var lonR2 = lon2 * Math.PI / 180;
    var latDifference = latR2 - latR1;
    var lonDifference = lonR2 - lonR1;
    var a = Math.pow(Math.sin(latDifference / 2), 2) + Math.cos(latR1) * Math.cos(latR2) * Math.pow(Math.sin(lonDifference / 2), 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(c * RADIUSKILOMETERS * 1000);
}

salesServices.factory('VendedoresService', ['$q', '$firebase', '$rootScope',
    function ($q, $firebase, $rootScope) {
        var vendedorDistancia;
        var universidades;
        var syncVendedores;

        return {
            findAll: function () {
                var ref = $rootScope.baseUrl;
                syncVendedores = $firebase(ref).$asArray();

                var promise = syncVendedores.$loaded().then(function (obj) {
                    syncVendedores.$watch(AtualizaDados);
                    console.log(obj);
                    return AtualizaDados();
                });

                function AtualizaDados() {
                    console.log("Changed", syncVendedores);
                    angular.forEach(syncVendedores, function (val) {
                        var vendedor = val;
                        vendedorDistancia = CalcRadiusDistance(myLat, myLng, vendedor.latitude, vendedor.longitude);
                        vendedor.distancia = vendedorDistancia;
                    })
                    return syncVendedores;
                }

                return promise;
            },

            findById: function (vendedorId) {
                console.log("ID: ", vendedorId);
                var vendedor;
                angular.forEach(syncVendedores, function (val) {
                    if (val.$id == vendedorId) {
                        vendedor = val;
                        console.log(vendedor.nome);
                        if (vendedor.ddd == 48) {
                            vendedor.ddd = "";
                        }
                    }
                })
                return vendedor;
            },

            getVendedoresOff: function () {
                var defer = $q.defer();
                if (Vendedores) {
                    defer.resolve(Vendedores);
                }
                else {
                    $http.get('data/sales/vendedores.json')
                        .success(function (data) {
                            Vendedores = data;
                            defer.resolve(Vendedores);
                        })
                        .error(function () {
                            defer.reject('could not find someFile.json');
                        });
                }
                return defer.promise;
            },

            getUniversidades: function () {
                var defer = $q.defer();
                if (!universidades) {
                    universidades = [];
                }
                syncVendedores.forEach(function (el) {
                    if (universidades.indexOf(el.universidade) == -1) {
                        universidades.push(el.universidade);
                    }
                });
                defer.resolve(universidades);
                return defer.promise;
            }
        };
    }]);