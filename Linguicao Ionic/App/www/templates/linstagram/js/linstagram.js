//var ref = new Firebase("https://linguicaoautomacao.firebaseio.com/");
var ref = new Firebase("https://uploadimage.firebaseio.com/");
var linstagramController = angular.module('linstagram.controller', [])

linstagramController.controller('CadastroController', function ($scope, $state, $firebaseAuth) {
    var fbAuth = $firebaseAuth(ref);

    $scope.login = function (username, password) {
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function (authData) {
            $state.go("app.linstagram");
        }).catch(function (error) {
            console.error("ERROR: " + error);
        });
    }

    $scope.register = function (username, password) {
        fbAuth.$createUser({email: username, password: password}).then(function (userData) {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function (authData) {
            $state.go("app.linstagram");
        }).catch(function (error) {
            console.error("ERROR: " + error);
        });
    }
})

linstagramController.controller('LinstagramController', function ($scope, $ionicHistory, $firebaseArray, $cordovaCamera) {
    $scope.images = [];
    var fbAuth = ref.getAuth();
    if (fbAuth) {
        var userReference = ref.child("users/" + fbAuth.uid);
        //var syncArray = $firebaseArray(userReference.child("images"));
        var syncArray = $firebaseArray(ref.child("users"));
        syncArray.$loaded().then(function (obj) {
            console.log(obj);
        });
        $scope.images = syncArray;
    }
    else {
        $state.go("app.cadastro");
    }

    $scope.upload = function () {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            cameraDirection: 1,
            correctOrientation: true,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function (imageData) {
            $firebaseArray(userReference.child("images")).$add({image: imageData}).then(function () {
                alert("Image has been uploaded");
            });
        }, function (error) {
            console.error(error);
        });
    }

    $ionicHistory.clearHistory();
})