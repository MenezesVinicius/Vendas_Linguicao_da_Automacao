var salesServices = angular.module('sales.services', ['ionic', 'ngCordova']);
var myLat = 0;
var myLng = 0;

/**
 * @return {number}
 */
function CalcRadiusDistance(lat1, lon1, lat2, lon2) {
    var RADIUSKILOMETERS = 6373;
    var latR1 = lat1 * Math.PI / 180;
    var lonR1 = lon1 * Math.PI / 180;
    var latR2 = lat2 * Math.PI / 180;
    var lonR2 = lon2 * Math.PI / 180;
    var latDifference = latR2 - latR1;
    var lonDifference = lonR2 - lonR1;
    var a = Math.pow(Math.sin(latDifference / 2), 2) + Math.cos(latR1) * Math.cos(latR2) * Math.pow(Math.sin(lonDifference / 2), 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(c * RADIUSKILOMETERS * 1000);
}

salesServices.factory('VendedoresService', ['$q', '$firebaseArray', '$http', '$cordovaToast', '$timeout', '$cordovaContacts',
    function ($q, $firebaseArray, $http, $cordovaToast, $timeout, $cordovaContacts) {
        var vendedorDistancia;
        var universidades;
        var syncVendedores;

        return {
            findAll: function () {
                var defer = $q.defer();
                var timer = $timeout(
                    function () {
                        defer.reject("Error Firebase");
                    },
                    20000
                );

                var ref = new Firebase("https://linguicaoautomacao.firebaseio.com/");
                syncVendedores = $firebaseArray(ref);

                syncVendedores.$loaded().then(function (obj) {
                    syncVendedores.$watch(AtualizaDados);
                    $timeout.cancel(timer);
                    return AtualizaDados();
                });

                function AtualizaDados() {
                    console.log("Changed", syncVendedores);
                    angular.forEach(syncVendedores, function (val) {
                        var vendedor = val;
                        vendedorDistancia = CalcRadiusDistance(myLat, myLng, vendedor.latitude, vendedor.longitude);
                        vendedor.distancia = vendedorDistancia;
                    })
                    $timeout.cancel(timer);
                    return defer.resolve(syncVendedores);
                }

                return defer.promise;
            },

            findByName: function (vendedorName) {
                console.log("ID: ", vendedorName);
                var vendedor;
                angular.forEach(vendedores, function (val) {
                    if (val.nome == vendedorName) {
                        vendedor = val;
                    }
                })
                return vendedor;
            },

            getVendedoresOff: function () {
                var defer = $q.defer();

                $http.get('data/sales/vendedores.json')
                    .success(function (data) {
                        syncVendedores = data;
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.reject('could not find someFile.json');
                    });

                return defer.promise;
            },

            getUniversidades: function () {
                var defer = $q.defer();
                if (!universidades) {
                    universidades = [];
                }
                console.log(syncVendedores);
                angular.forEach(syncVendedores, function (el) {
                    if (universidades.indexOf(el.universidade) == -1) {
                        universidades.push(el.universidade);
                    }
                });
                defer.resolve(universidades);
                return defer.promise;
            },

            getContato: function (tel) {
                var defer = $q.defer();
                var contato;
                $cordovaContacts.find({filter: ''}).then(function(contacts) {
                    for (var i = 0; i < contacts.length; i++) {
                        for (var j = 0; j < contacts[i].phoneNumbers.length; j++) {
                            var telContato = contacts[i].phoneNumbers[j].value.toString().replace(/\s/g, '');
                            if(tel.indexOf(telContato) != -1) {
                                contato = telContato;
                            }
                            else if(telContato.indexOf(tel) != -1) {
                                contato = telContato;
                            }
                        }
                    }
                    defer.resolve(contato);
                }, function(error) {
                    defer.reject();
                });
                return defer.promise;
            },

            saveContato: function(tel, nome){
                var defer = $q.defer();
                var contactForm = {
                    "displayName": nome,
                    "name": {
                        "givenName": nome,
                        "formatted": nome
                    },
                    "nickname": null,
                    "phoneNumbers": [
                        {
                            "value": tel,
                            "type": "mobile"
                        }
                    ]
                };

                $cordovaContacts.save(contactForm).then(function(result) {
                    defer.resolve(result);
                }, function(err) {
                    defer.reject();
                });

                return defer.promise;
            }
        };
    }]);