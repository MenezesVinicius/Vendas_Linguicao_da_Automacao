var showsService = angular.module('shows.services', []);

showsService.factory('ShowService', function($q, $http) {

    function Show(nome, time) {
        this.name = name;
        this.time = time;
    }

    var showsPacha = function() {

        var defer = $q.defer();

        $http.get('data/bandas-pacha.json')
            .success(function(data) {
                defer.resolve(data);
            })
            .error(function() {
                console.log('could not find file');
                defer.reject('could not find someFile.json');
            });

        return defer.promise;
    };

    var showsStage = function() {

        var defer = $q.defer();

        $http.get('data/bandas-stage.json')
            .success(function(data) {
                defer.resolve(data);
            })
            .error(function() {
                console.log('could not find file');
                defer.reject('could not find someFile.json');
            });

        return defer.promise;
    };

    return {
        getShowsStage: function() {
            return showsStage();
        },
        getShowsPacha: function() {
            return showsPacha();
        }
    };
});
