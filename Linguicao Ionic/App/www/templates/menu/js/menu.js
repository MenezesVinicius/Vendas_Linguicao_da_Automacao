angular.module('menu.controller', ['helpers.dataloader'])
    .controller('MenuController',
    function ($scope) {

        var patrocinadores = ['aurora.png',
            'babel.jpg',
            'bomdebrasa.png',
            'container.png',
            'natatorium.png',
            'topline.png',
            'CAECA.png',
            'travel.png',
            'glx.png',
            'devassa.png',
            'ATACA.png'];

        $scope.patrocinadores = patrocinadores;
    });