f = open("vendedores.tsv")
f2 = open("vendedores.json","w")

tags = ["nome", "telefone", "email", "universidade", "curso", "img_url", "fb_link"]
lines = []
for line in f:
    values = line.strip().split("\t")
    def writeTag(tag, value):
	return "\"%s\":\"%s\"" % (tag, value)
    
    lines.append("{"+",".join([writeTag(t,v) for t,v in zip(tags,values)])+"}")

f2.write("["+",\n".join(lines)+"]")   
